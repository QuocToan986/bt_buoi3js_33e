// Bài 1: Tính lương nhân viên
function tinhTien() {
  var luongValue = document.getElementById("txt_luong_1_ngay").value * 1;
  var ngayLamValue = document.getElementById("txt_so_ngay_lam").value * 1;
  var tongTienValue = luongValue * ngayLamValue;
  document.getElementById(
    "result"
  ).innerHTML = `Lương nhân viên: ${tongTienValue.toLocaleString()} VND`;
}

// Bài 2: Tính giá trị tb của 5 số nhập vào
function inRa() {
  var so1Value = document.getElementById("txt_so1").value * 1;
  var so2Value = document.getElementById("txt_so2").value * 1;
  var so3Value = document.getElementById("txt_so3").value * 1;
  var so4Value = document.getElementById("txt_so4").value * 1;
  var so5Value = document.getElementById("txt_so5").value * 1;
  var tongTBValue = (so1Value + so2Value + so3Value + so4Value + so5Value) / 5;
  console.log("tongTBValue: ", tongTBValue);
  document.getElementById("total").innerHTML = `Kết quả = ${tongTBValue}`;
}

// Bài 3: Quy đổi tiền đô ra vnd
const tienVND = 23.0;
function doiTien() {
  var tienDoi = document.getElementById("txt_tien_doi").value * 1;
  var quyDoi = tienDoi * tienVND;
  document.getElementById(
    "thongBaoTien"
  ).innerHTML = `Số tiền: ${quyDoi.toFixed(3)} VND`;
}

//Bài 4: Tính chu vi - diện tích hình chữ nhật
function tinhPhepTinh() {
  var chieuDaiValue = document.getElementById("txt_chieu_dai").value * 1;
  var chieuRongValue = document.getElementById("txt_chieu_rong").value * 1;
  var chuViValue = (chieuDaiValue + chieuRongValue) * 2;
  var dienTichValue = chieuDaiValue * chieuRongValue;

  document.getElementById("txt_chu_vi").value = chuViValue.toLocaleString();
  document.getElementById("txt_dien_tich").value =
    dienTichValue.toLocaleString();
}

// Bài 5: Tính tổng 2 ký số có 2 chữ số
function tinhTong() {
  var kySoValue = document.getElementById("txt_ky_so").value * 1;
  var soHangDonViValue = kySoValue % 10;
  var soHangChucValue = kySoValue / 10;
  var tong = soHangDonViValue + soHangChucValue;
  document.getElementById("show").innerHTML = `Tổng = ${Math.floor(tong)}`;
}
